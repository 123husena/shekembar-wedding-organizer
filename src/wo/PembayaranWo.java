/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wo;

import java.awt.Point;
import java.io.File;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import koneksi.koneksi;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author FITRI RHAMADANI
 */
public class PembayaranWo extends javax.swing.JFrame {
    private Connection conn = Koneksi.getKoneksi();
    private static Point point = new Point();
    private DefaultTableModel tabmode;
    private String idpembayaran,idpemesanan,id,id_pembayaran,total_bayar,dp,status_pembayaran,id_pelanggan,tgl_pembayaran,id_pegawai,jumlah_bayar,sisa_bayar = null;
    private String totalpemesanan = null;
    JasperReport jasperReport;
    JasperDesign jasperDesign;
    JasperPrint jasperPrint;        
    /**
     * Creates new form Booking
     */
    public PembayaranWo() {
        initComponents();
        ambil_pemesanan(false,null);
        ambil_pegawai();
        datatable();       
    }
    protected void kosong(){
        totalpemesanan = null;
        jComboBox1.setSelectedIndex(0);
        jComboBox2.setSelectedIndex(0);  
        jComboBox3.setSelectedIndex(0);  
        tdppembayaran.setText("");
        ttglpembayaran.setCalendar(null);
        tlokasirak.setText("");
        tjmlpembayaran.setText("");
        tcari.setText("");
    } 
    protected void caridata (String key){
        Object[] Baris ={"ID","ID Pembayaran","ID Pemesanan","ID Pelanggan","ID Pegawai","DP","Jumlah Bayar", "Sisa Bayar","Total Pembayaran","Tanggal Pembayaran","Status pembayaran"};
        tabmode = new DefaultTableModel (null, Baris);
        tablerak.setModel(tabmode);
        String sql = "SELECT pembayaran.`id` AS pembayaran_id, pembayaran.`id_pembayaran` AS pembayaran_id_pembayaran, pemesanan.`id_pemesanan` AS "
                + "pemesanan_id_pemesanan, pemesanan.`id_pelanggan` AS pemesanan_id_pelanggan, pembayaran.`id_pegawai` AS pembayaran_id_pegawai, pembayaran.`dp` AS pembayaran_dp, pembayaran.`jumlah_bayar` AS pembayaran_jumlah_bayar, pembayaran.`sisa_bayar` AS pembayaran_sisa_bayar, pemesanan.`total` AS pemesanan_total, pembayaran.`tgl_pembayaran` AS pembayaran_tgl_pembayaran, pembayaran.`status_pembayaran` AS pembayaran_status_pembayaran FROM `pembayaran` pembayaran INNER JOIN `pemesanan` "
                + "pemesanan ON pembayaran.`id_pemesanan` = pemesanan.`id_pemesanan` where pembayaran.`id_pembayaran` like '%"+key+"%'";
        try {
            java.sql.Statement stat = conn.createStatement();
            java.sql.ResultSet hasil = stat.executeQuery(sql);
            while (hasil.next()){
                String id = hasil.getString("pembayaran_id");                
                String a = hasil.getString("pembayaran_id_pembayaran");
                String b = hasil.getString("pemesanan_id_pemesanan");
                String c = hasil.getString("pemesanan_id_pelanggan");
                String d = hasil.getString("pembayaran_id_pegawai");
                String e = hasil.getString("pembayaran_dp");
                String f = hasil.getString("pembayaran_jumlah_bayar");
                String g = hasil.getString("pembayaran_sisa_bayar");
                String h = hasil.getString("pemesanan_total");
                String i = hasil.getString("pembayaran_tgl_pembayaran");
                String j = hasil.getString("pembayaran_status_pembayaran");
                
                String[] data={id,a,b,c,d,e,f,g,h,i,j};
                tabmode.addRow(data);
            }
        }catch (Exception e) {
            System.out.print(e);
        }
    }
    
    public void ambil_pegawai(){
        try{
            String sql = "Select * from pegawai";
            java.sql.Statement stat = conn.createStatement();
            java.sql.ResultSet hasil = stat.executeQuery(sql);

                jComboBox3.addItem("== Pilih pegawai ==");
                while(hasil.next()){
                    jComboBox3.addItem(hasil.getString("id_pegawai"));
                }
                hasil.last();
                hasil.first();
        }catch(SQLException e){
            System.out.print(e);
        }        
    }    
    
    public void ambil_pemesanan(boolean justOne, String id){
        try{
            String sql = null;
            if(justOne && id != null){
                sql = "select * from pemesanan WHERE id_pemesanan= '"+id+"'";                
            }else{
                sql = "Select * from pemesanan";
            }
            java.sql.Statement stat = conn.createStatement();
            java.sql.ResultSet hasil = stat.executeQuery(sql);
            if(justOne == true){
                while(hasil.next()){
                    totalpemesanan = hasil.getInt("total")+"";
                    tlokasirak.setText(totalpemesanan);
                }                
            }else{
                jComboBox1.addItem("== Pilih pemesanan ==");
                while(hasil.next()){
                    jComboBox1.addItem(hasil.getString("id_pemesanan"));
                }
                hasil.last();
                hasil.first();                
            }
        }catch(Exception e){
            System.out.print(e);
        }        
    }     
    public void switchUbah(boolean switchs){
        jComboBox1.setEnabled(switchs);
        jComboBox3.setEnabled(switchs);
        tdppembayaran.setEnabled(switchs);
        ttglpembayaran.setEnabled(switchs);
        bsimpan.setEnabled(switchs);
        bbersih.setEnabled(switchs);
    }
    
    protected void datatable (){
        Object[] Baris ={"ID","ID Pembayaran","ID Pemesanan","ID Pelanggan","ID Pegawai","DP","Jumlah Bayar", "Sisa Bayar","Total Pembayaran","Tanggal Pembayaran","Status pembayaran"};
        tabmode = new DefaultTableModel (null, Baris);
        tablerak.setModel(tabmode);
        String sql = "SELECT pembayaran.`id` AS pembayaran_id, pembayaran.`id_pembayaran` AS pembayaran_id_pembayaran, pemesanan.`id_pemesanan` AS "
                + "pemesanan_id_pemesanan, pemesanan.`id_pelanggan` AS pemesanan_id_pelanggan, pembayaran.`id_pegawai` AS pembayaran_id_pegawai, pembayaran.`dp` AS pembayaran_dp, pembayaran.`jumlah_bayar` AS pembayaran_jumlah_bayar, pembayaran.`sisa_bayar` AS pembayaran_sisa_bayar, pembayaran.`total_bayar` AS pembayaran_total_bayar, pemesanan.`total` AS pemesanan_total, pembayaran.`tgl_pembayaran` AS pembayaran_tgl_pembayaran, pembayaran.`status_pembayaran` AS pembayaran_status_pembayaran FROM `pembayaran` pembayaran INNER JOIN `pemesanan` "
                + "pemesanan ON pembayaran.`id_pemesanan` = pemesanan.`id_pemesanan`";
        try {
            java.sql.Statement stat = conn.createStatement();
            java.sql.ResultSet hasil = stat.executeQuery(sql);
            while (hasil.next()){
                String id = hasil.getString("pembayaran_id");                
                String a = hasil.getString("pembayaran_id_pembayaran");
                String b = hasil.getString("pemesanan_id_pemesanan");
                String c = hasil.getString("pemesanan_id_pelanggan");
                String d = hasil.getString("pembayaran_id_pegawai");
                String e = hasil.getString("pembayaran_dp");
                String f = hasil.getString("pembayaran_jumlah_bayar");
                String g = hasil.getString("pembayaran_sisa_bayar");
                String h = hasil.getString("pembayaran_total_bayar");
                String i = hasil.getString("pembayaran_tgl_pembayaran");
                String j = hasil.getString("pembayaran_status_pembayaran");
                
                String[] data={id, a,b,c,d,e,f,g,h,i,j};
                tabmode.addRow(data);
            }
        }catch (Exception e) {
            System.out.print(e);
        }
    }
       
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        bkembali = new javax.swing.JButton();
        ttglpembayaran = new com.toedter.calendar.JDateChooser();
        jLabel4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablerak = new javax.swing.JTable();
        bbersih = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        tlokasirak = new javax.swing.JTextField();
        bsimpan = new javax.swing.JButton();
        tcari = new javax.swing.JTextField();
        bhapus = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        tdppembayaran = new javax.swing.JTextField();
        jComboBox2 = new javax.swing.JComboBox<>();
        tjmlpembayaran = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jComboBox3 = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        bubah = new javax.swing.JButton();
        bcancel = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        bcetakone = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setSize(new java.awt.Dimension(900, 610));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setForeground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(204, 153, 254));
        jPanel1.setFont(new java.awt.Font("Arial Narrow", 1, 28)); // NOI18N

        jLabel6.setFont(new java.awt.Font("Arial Narrow", 1, 30)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Form Pembayaran");

        bkembali.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        bkembali.setText("X");
        bkembali.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bkembaliActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel6)
                .addGap(269, 269, 269)
                .addComponent(bkembali)
                .addGap(22, 22, 22))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(bkembali, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        ttglpembayaran.setDateFormatString("yyyy-MM-dd");

        jLabel4.setText("Id Pemesanan");

        jLabel2.setText("DP");

        tablerak.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablerak.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablerakMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablerak);

        bbersih.setText("Bersihkan");
        bbersih.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bbersihActionPerformed(evt);
            }
        });

        jLabel3.setText("Total Bayar");

        tlokasirak.setEditable(false);
        tlokasirak.setEnabled(false);
        tlokasirak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tlokasirakActionPerformed(evt);
            }
        });

        bsimpan.setText("Simpan");
        bsimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bsimpanActionPerformed(evt);
            }
        });

        tcari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tcariActionPerformed(evt);
            }
        });
        tcari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tcariKeyReleased(evt);
            }
        });

        bhapus.setText("Hapus");
        bhapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bhapusActionPerformed(evt);
            }
        });

        jLabel5.setText("Tanggal Pembayaran ");

        jLabel7.setText("Status Pembayaran");

        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        jLabel8.setText("Cari");

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- Pilih Status --", "Lunas", "Belum Lunas" }));

        tjmlpembayaran.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tjmlpembayaranActionPerformed(evt);
            }
        });

        jLabel9.setText("Jumlah Bayar");

        jComboBox3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox3ActionPerformed(evt);
            }
        });

        jLabel10.setText("Id Pegawai");

        bubah.setText("Ubah");
        bubah.setEnabled(false);
        bubah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bubahActionPerformed(evt);
            }
        });

        bcancel.setText("Kembali");
        bcancel.setEnabled(false);
        bcancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bcancelActionPerformed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel12.setText("*Pencarian hanya berdasarkan ID Pembayaran");

        bcetakone.setText("Cetak");
        bcetakone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bcetakoneActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5)
                            .addComponent(ttglpembayaran, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)
                            .addComponent(tlokasirak, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(36, 36, 36)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(tdppembayaran, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7)
                                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9)
                                    .addComponent(tjmlpembayaran, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(bbersih, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(bsimpan, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(bubah, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(bcancel, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(bcetakone, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(bhapus, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(12, 12, 12)
                                .addComponent(tcari, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(tcari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel12)))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bubah)
                    .addComponent(bcancel)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(bcetakone)
                        .addComponent(bhapus)))
                .addGap(37, 37, 37)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel4)
                                .addComponent(jLabel2))
                            .addComponent(jLabel7))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tdppembayaran, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel3)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel9)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ttglpembayaran, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tlokasirak, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tjmlpembayaran, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bbersih)
                    .addComponent(bsimpan, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bkembaliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bkembaliActionPerformed
        // TODO add your handling code here:
        new Master().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_bkembaliActionPerformed

    private void tablerakMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablerakMouseClicked
        int baris = tablerak.getSelectedRow();
        tlokasirak.setText(tabmode.getValueAt(baris, 7).toString());
        jComboBox2.setSelectedItem(tabmode.getValueAt(baris, 10).toString());
        // selecting data //
        id = tabmode.getValueAt(baris, 0).toString();
        idpembayaran = tabmode.getValueAt(baris, 1).toString();
        idpemesanan =  tabmode.getValueAt(baris, 2).toString();
        total_bayar =  tabmode.getValueAt(baris, 3).toString();
        dp =  tabmode.getValueAt(baris, 5).toString();
        status_pembayaran = tabmode.getValueAt(baris, 10).toString();
        tgl_pembayaran = tabmode.getValueAt(baris, 9).toString();
        id_pegawai =tabmode.getValueAt(baris, 4).toString();
        jumlah_bayar = tabmode.getValueAt(baris, 6).toString();
        sisa_bayar = tabmode.getValueAt(baris, 7).toString();
        totalpemesanan = tabmode.getValueAt(baris, 7).toString();

        switchUbah(false);
        bubah.setEnabled(true);
        bcancel.setEnabled(true);
        //        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        //        try {
            //            ttglpembayaran.setDate(formatter.parse(tabmode.getValueAt(baris, 8).toString()));
            //        } catch (ParseException ex) {
            //            Logger.getLogger(pembayaran.class.getName()).log(Level.SEVERE, null, ex);
            //        }
    }//GEN-LAST:event_tablerakMouseClicked

    private void bbersihActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bbersihActionPerformed
        kosong();        // TODO add your handling code here:
        try {
            Util.dateIndonesia();
        } catch (ParseException ex) {
            Logger.getLogger(PembayaranWo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_bbersihActionPerformed

    private void tlokasirakActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tlokasirakActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tlokasirakActionPerformed

    private void bsimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bsimpanActionPerformed
        try {
            if(jComboBox1.getSelectedIndex() == 0 || tlokasirak.getText().equals("")){
                JOptionPane.showMessageDialog(null, "Mohon isi ID pemesanan terlebih dahulu!");
            }else if(jComboBox3.getSelectedIndex() == 0){
                JOptionPane.showMessageDialog(null, "Mohon pilih Data Pegawai terlebih dahulu!");
            }else if(tdppembayaran.getText().equals("")){
                JOptionPane.showMessageDialog(null, "Mohon isi DP Pembayaran terlebih dahulu!");
            }else if(ttglpembayaran.getDate() == null){
                JOptionPane.showMessageDialog(null, "Mohon isi Tanggal Pembayaran terlebih dahulu!");
            }
            else if(jComboBox2.getSelectedIndex() == 0){
                JOptionPane.showMessageDialog(null, "Mohon isi status pembayaran terlebih daulu!");
            }else if(tjmlpembayaran.getText().equals("")){
                JOptionPane.showMessageDialog(null, "mohon pilih jumlah bayar terlebih dahulu!");
            }else{
                PreparedStatement stat = conn.prepareStatement("insert into pembayaran (id_pembayaran,total_bayar,dp,status_pembayaran,tgl_pembayaran,id_pegawai,id_pemesanan,jumlah_bayar,sisa_bayar) values (?,?,?,?,?,?,?,?,?)");
                stat.setString(1, "PBY-"+Util.getSaltString());
                stat.setString(2, totalpemesanan);
                stat.setString(3, tdppembayaran.getText());
                stat.setString(4, jComboBox2.getSelectedItem().toString());
                stat.setString(5, ((JTextField)ttglpembayaran.getDateEditor().getUiComponent()).getText());
                stat.setString(6, jComboBox3.getSelectedItem().toString());
                stat.setString(7, jComboBox1.getSelectedItem().toString());
                stat.setString(8, tjmlpembayaran.getText());
                stat.setString(9, (Integer.parseInt(totalpemesanan) - (Integer.parseInt(tjmlpembayaran.getText()) + Integer.parseInt(tdppembayaran.getText())))+"");
                stat.executeUpdate();
                JOptionPane.showMessageDialog(null, "Data berhasil disimpan", "Pesan", JOptionPane.INFORMATION_MESSAGE);
                datatable();
                kosong();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_bsimpanActionPerformed

    private void tcariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tcariActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tcariActionPerformed

    private void tcariKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tcariKeyReleased
        String key=tcari.getText();
        System.out.println(key);

        if(!"".equals(key)){
            caridata(key);
        }else{
            datatable();
        }         // TODO add your handling code here:
    }//GEN-LAST:event_tcariKeyReleased
    public void doneselect(){
        switchUbah(true);
        bubah.setEnabled(false);
        bcancel.setEnabled(false);
        totalpemesanan =idpembayaran= idpemesanan= id_pembayaran= total_bayar= dp= status_pembayaran= id_pelanggan= tgl_pembayaran= id_pegawai= jumlah_bayar= sisa_bayar = null;
        kosong();          
        
    }
    private void bhapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bhapusActionPerformed
        int confirm = JOptionPane.showConfirmDialog(null, "Apakah anda yakin ingin menghapus data tersebut?", "Konfirmasi", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (confirm == 0) {
            if(idpembayaran != null){
                try {
                    java.sql.PreparedStatement stat = conn.prepareStatement("delete from pembayaran where id_pembayaran ='" + idpembayaran + "'");
                    stat.executeUpdate();
                    JOptionPane.showMessageDialog(null, "Data berhasil dihapus", "Pesan", JOptionPane.INFORMATION_MESSAGE);
                    datatable();
                    doneselect();
                } catch (SQLException e) {
                }
            }else{
                JOptionPane.showMessageDialog(null, "Mohon Pilih data yang ingin dihapus terlebih dahulu!", "Pesan", JOptionPane.INFORMATION_MESSAGE);
            }

        }          // TODO add your handling code here:
    }//GEN-LAST:event_bhapusActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        if(jComboBox1.getSelectedIndex() == 0){
            tlokasirak.setText("");
        }else{
            ambil_pemesanan(true, jComboBox1.getSelectedItem().toString());
        }
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void tjmlpembayaranActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tjmlpembayaranActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tjmlpembayaranActionPerformed

    private void jComboBox3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox3ActionPerformed

    private void bubahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bubahActionPerformed
        // TODO add your handling code here:
        try {

            if(jComboBox2.getSelectedIndex() == 0){
                JOptionPane.showMessageDialog(null, "Mohon isi status pembayaran terlebih daulu!");
            }else if(tjmlpembayaran.getText().equals("")){
                JOptionPane.showMessageDialog(null, "mohon pilih jumlah bayar terlebih dahulu!");
            }else{
                PreparedStatement stat = conn.prepareStatement("insert into pembayaran (id_pembayaran,total_bayar,dp,status_pembayaran,tgl_pembayaran,id_pegawai,id_pemesanan,jumlah_bayar,sisa_bayar) values (?,?,?,?,?,?,?,?,?)");

                stat.setString(1, idpembayaran);
                stat.setString(2, tlokasirak.getText());
                stat.setString(3, dp);
                stat.setString(4, jComboBox2.getSelectedItem().toString());
                stat.setString(5, tgl_pembayaran);
                stat.setString(6, id_pegawai);
                stat.setString(7, idpemesanan);
                stat.setString(8, tjmlpembayaran.getText());
                stat.setString(9, (Integer.parseInt(totalpemesanan) - Integer.parseInt(tjmlpembayaran.getText()))+"");
                stat.executeUpdate();
                JOptionPane.showMessageDialog(null, "Data berhasil disimpan", "Pesan", JOptionPane.INFORMATION_MESSAGE);
                datatable();
                kosong();
                doneselect();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }//GEN-LAST:event_bubahActionPerformed

    private void bcancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bcancelActionPerformed
        // TODO add your handling code here:
        doneselect();
    }//GEN-LAST:event_bcancelActionPerformed

    private void bcetakoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bcetakoneActionPerformed
        // TODO add your handling code here:
        if(id == null){
            JOptionPane.showMessageDialog(null, "Mohon pilih data dari table terlebih dahulu!");
        }else{
            try {
                String RealPath = "lib/";
                HashMap hash = new HashMap();
                String NamaFile = "/report/pembayaran.jasper";
                Map<String, Object> map = new HashMap<>();
                String tgl = Util.tampilkanTanggalDanWaktu(new Date(), "EEEE, dd MMMM yyyy", null);
                String tgl_nolengkap = Util.dateIndonesia();
                map.put("tgl_indo", tgl_nolengkap);
                map.put("tgl_nolengkap", tgl_nolengkap);
                map.put("id_pembayaran", id);//parameter name should be like it was named inside your report.                
                map.put("RealPath", RealPath);
                InputStream report;
                report = getClass().getResourceAsStream(NamaFile);                
                jasperPrint = JasperFillManager.fillReport(report, map, conn);
                JasperViewer.viewReport(jasperPrint, false);
            } catch (JRException ex) {
                Logger.getLogger(PembayaranWo.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(PembayaranWo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_bcetakoneActionPerformed


    

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PembayaranWo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PembayaranWo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PembayaranWo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PembayaranWo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PembayaranWo().setVisible(true);
            }
        });
    }
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bbersih;
    private javax.swing.JButton bcancel;
    private javax.swing.JButton bcetakone;
    private javax.swing.JButton bhapus;
    private javax.swing.JButton bkembali;
    private javax.swing.JButton bsimpan;
    private javax.swing.JButton bubah;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JComboBox<String> jComboBox3;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablerak;
    private javax.swing.JTextField tcari;
    private javax.swing.JTextField tdppembayaran;
    private javax.swing.JTextField tjmlpembayaran;
    private javax.swing.JTextField tlokasirak;
    private com.toedter.calendar.JDateChooser ttglpembayaran;
    // End of variables declaration//GEN-END:variables
}
