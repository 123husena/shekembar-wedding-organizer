/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wo;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;

/**
 *
 * @author C030122001
 */
public class Util {
    public static String currencyID(String nominal){
        int nom = Integer.parseInt(nominal);
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        return formatRupiah.format(nom);
    }
    
    public static String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 8) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }
        
    protected static String tampilkanTanggalDanWaktu(Date tanggalDanWaktu,
            String pola, Locale lokal) {
        String tanggalStr = null;
        SimpleDateFormat formatter = null;
        if (lokal == null) {
            formatter = new SimpleDateFormat(pola);
        } else {
            formatter = new SimpleDateFormat(pola, new Locale("id", "ID"));
        }
 
        tanggalStr = formatter.format(tanggalDanWaktu);
        return tanggalStr;
    }  
    
    protected static String dateIndonesia() throws ParseException {
        Date date = new Date();
        String tanggalStr,tanggalDay,tanggalMonth,tanggalYear = null;
        SimpleDateFormat formatter,formatterDay,formatterYear,formatterMonth = null;
        formatter = new SimpleDateFormat("EEEE,", new Locale("id", "ID"));
        formatterDay = new SimpleDateFormat("d", new Locale("id", "ID"));
        formatterYear = new SimpleDateFormat("yyyy", new Locale("id", "ID"));
        formatterMonth = new SimpleDateFormat("MMMM", new Locale("id", "ID"));        
        tanggalStr = formatter.format(date);
        tanggalYear = formatterYear.format(date);
        tanggalMonth = formatterMonth.format(date);
        tanggalDay = formatterDay.format(date);
         return tanggalStr+ tanggalDay  +" " + tanggalMonth + " " + tanggalYear; // 2015-03-03
    }      
}
