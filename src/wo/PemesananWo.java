/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wo;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.File;
import java.io.InputStream;
import java.sql.*;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import koneksi.koneksi;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author DELL
 */
public class PemesananWo extends javax.swing.JDialog {
    private Connection con = Koneksi.getKoneksi();
    private static Point point = new Point();
    private DefaultTableModel tabmode;
    String id_pemesanan = "";
        JasperReport jasperReport;
    JasperDesign jasperDesign;
    JasperPrint jasperPrint;
    public PemesananWo(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        setSize(825, 367);
        setResizable(false);
        setLocationRelativeTo(null);
        this.setUndecorated(true); 
        this.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                point.x = e.getX();
                point.y = e.getY();
            }
        });
        this.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent e) {
                Point p = getLocation();
                setLocation(p.x + e.getX() - point.x, p.y + e.getY() - point.y);
            }
        });
        initComponents();
        datatable();
    }
    protected void datatable (){
        Object[] Baris ={"Id Pemesanan","Tanggal Pemesanan","Tanggal Pernikahan","Nama Pemesan","Nama Pegawai","No.HP Pegawai"};
        tabmode = new DefaultTableModel (null, Baris);
        tablevendor.setModel(tabmode);
        String sql = "SELECT pemesanan.`id_pemesanan` AS pemesanan_id_pemesanan, pemesanan.`tgl_pemesanan` AS "
                + "pemesanan_tgl_pemesanan, pemesanan.`tgl_pernikahan` AS pemesanan_tgl_pernikahan, pemesanan.`id_pelanggan` AS pemesanan_id_pelanggan, pemesanan.`id_pegawai` AS pemesanan_id_pegawai, pemesanan.`id_gedung` AS pemesanan_id_gedung, pemesanan.`id_dekorasi` AS pemesanan_id_dekorasi, pemesanan.`id_catering` AS pemesanan_id_catering, pemesanan.`porsi_catering` AS pemesanan_porsi_catering, pemesanan.`id_fotografer` AS pemesanan_id_fotografer, "
                + "pemesanan.`id_mua` AS pemesanan_id_mua, pemesanan.`id_entertaiment` AS pemesanan_id_entertaiment, pegawai.`id_pegawai` AS pegawai_id_pegawai, pegawai.`username` AS pegawai_username, pegawai.`nama_pegawai` AS pegawai_nama_pegawai, pegawai.`notelp_pegawai` AS pegawai_notelp_pegawai, pegawai.`email_pegawai` AS pegawai_email_pegawai, pegawai.`password` AS pegawai_password, pelanggan.`id_pelanggan` AS pelanggan_id_pelanggan, pelanggan.`nama_pelanggan` AS pelanggan_nama_pelanggan, pelanggan.`nik_pelanggan` AS pelanggan_nik_pelanggan, pelanggan.`alamat_pelanggan` AS pelanggan_alamat_pelanggan, pelanggan.`nohp_pelanggan` AS pelanggan_nohp_pelanggan, pelanggan.`email_pelanggan` AS pelanggan_email_pelanggan, pelanggan.`tgl_registrasi` AS pelanggan_tgl_registrasi "
                + "FROM `pemesanan` pemesanan INNER JOIN `pegawai` pegawai ON pemesanan.`id_pegawai` = pegawai.`id_pegawai` INNER JOIN `pelanggan` pelanggan ON pemesanan.`id_pelanggan` = pelanggan.`id_pelanggan`";
        try {
            java.sql.Statement stat = con.createStatement();
            java.sql.ResultSet hasil = stat.executeQuery(sql);
            while (hasil.next()){
                String a = hasil.getString("pemesanan_id_pemesanan");
                String b = hasil.getString("pemesanan_tgl_pemesanan");
                String c = hasil.getString("pemesanan_tgl_pernikahan");
                String d = hasil.getString("pegawai_nama_pegawai");
                String e = hasil.getString("pelanggan_nama_pelanggan");
                String f = hasil.getString("pelanggan_nohp_pelanggan");                
                
                String[] data={a,b,c,d,e,f};
                tabmode.addRow(data);
            }
        }catch (Exception e) {
            System.out.println(e);
        }
    }
    protected void caridata (String key){
        Object[] Baris ={"Id Pemesanan","Tanggal Pemesanan","Tanggal Pernikahan","Nama Pemesan","Nama Pegawai","No.HP Pegawai"};
        tabmode = new DefaultTableModel (null, Baris);
        tablevendor.setModel(tabmode);
        String sql = "SELECT pemesanan.`id_pemesanan` AS pemesanan_id_pemesanan, pemesanan.`tgl_pemesanan` AS pemesanan_tgl_pemesanan, pemesanan.`tgl_pernikahan` AS pemesanan_tgl_pernikahan, pemesanan.`id_pelanggan` AS pemesanan_id_pelanggan, pemesanan.`id_pegawai` AS pemesanan_id_pegawai, pemesanan.`id_gedung` AS pemesanan_id_gedung, pemesanan.`id_dekorasi` AS pemesanan_id_dekorasi, pemesanan.`id_catering` AS pemesanan_id_catering, pemesanan.`porsi_catering` AS pemesanan_porsi_catering, pemesanan.`id_fotografer` AS pemesanan_id_fotografer, pemesanan.`id_mua` AS pemesanan_id_mua, pemesanan.`id_entertaiment` AS pemesanan_id_entertaiment, pegawai.`id_pegawai` AS pegawai_id_pegawai, pegawai.`username` AS pegawai_username, pegawai.`nama_pegawai` AS pegawai_nama_pegawai, pegawai.`notelp_pegawai` AS pegawai_notelp_pegawai, pegawai.`email_pegawai` AS pegawai_email_pegawai, pegawai.`password` AS pegawai_password, pelanggan.`id_pelanggan` AS pelanggan_id_pelanggan, pelanggan.`nama_pelanggan` AS pelanggan_nama_pelanggan, pelanggan.`nik_pelanggan` AS pelanggan_nik_pelanggan, pelanggan.`alamat_pelanggan` AS pelanggan_alamat_pelanggan, pelanggan.`nohp_pelanggan` AS pelanggan_nohp_pelanggan, pelanggan.`email_pelanggan` AS pelanggan_email_pelanggan, pelanggan.`tgl_registrasi` AS pelanggan_tgl_registrasi " +
"FROM `pemesanan` pemesanan INNER JOIN `pegawai` pegawai ON pemesanan.`id_pegawai` = pegawai.`id_pegawai` INNER JOIN `pelanggan` pelanggan ON pemesanan.`id_pelanggan` = pelanggan.`id_pelanggan` where pemesanan.`id_pemesanan` like '%"+key+"%'";
        try {
            java.sql.Statement stat = con.createStatement();
            ResultSet hasil = stat.executeQuery(sql);
            while (hasil.next()){
                String a = hasil.getString("pemesanan_id_pemesanan");
                String b = hasil.getString("pemesanan_tgl_pemesanan");
                String c = hasil.getString("pemesanan_tgl_pernikahan");
                String d = hasil.getString("pegawai_nama_pegawai");
                String e = hasil.getString("pelanggan_nama_pelanggan");
                String f = hasil.getString("pelanggan_nohp_pelanggan");                
                
                String[] data={a,b,c,d,e,f};
                tabmode.addRow(data);
            }
        }catch (Exception e) {
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        bkembali = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        bhapus = new javax.swing.JButton();
        bhapus1 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        tcari = new javax.swing.JTextField();
        jScrollPane4 = new javax.swing.JScrollPane();
        tablevendor = new javax.swing.JTable();
        jToolBar1 = new javax.swing.JToolBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(204, 153, 254));
        jPanel1.setFont(new java.awt.Font("Arial Narrow", 1, 28)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Arial Narrow", 1, 30)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Daftar Pemesanan");

        bkembali.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        bkembali.setText("X");
        bkembali.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bkembaliActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(292, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(214, 214, 214)
                .addComponent(bkembali)
                .addGap(30, 30, 30))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(bkembali, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, -1));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setForeground(new java.awt.Color(255, 255, 255));

        bhapus.setText("Hapus");
        bhapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bhapusActionPerformed(evt);
            }
        });

        bhapus1.setText("Cetak");
        bhapus1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bhapus1ActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Century Schoolbook", 0, 14)); // NOI18N
        jLabel6.setText("Cari");

        tcari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tcariActionPerformed(evt);
            }
        });
        tcari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tcariKeyReleased(evt);
            }
        });

        tablevendor.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablevendor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablevendorMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tablevendor);

        jToolBar1.setRollover(true);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(0, 2, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(tcari, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(504, 504, 504))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 760, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(bhapus)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bhapus1)
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGap(460, 475, Short.MAX_VALUE)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 302, Short.MAX_VALUE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(101, 101, 101)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tcari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bhapus)
                    .addComponent(bhapus1))
                .addContainerGap(18, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGap(0, 15, Short.MAX_VALUE)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 403, Short.MAX_VALUE)))
        );

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 790, 420));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tablevendorMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablevendorMouseClicked
        int baris = tablevendor.getSelectedRow();
        id_pemesanan = tabmode.getValueAt(baris, 0).toString();
    }//GEN-LAST:event_tablevendorMouseClicked

    private void tcariKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tcariKeyReleased
        String key=tcari.getText();
        if(!"".equals(key)){
            caridata(key);
        }else{
            datatable();
        }     // TODO add your handling code here:
    }//GEN-LAST:event_tcariKeyReleased
    
    private void tcariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tcariActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tcariActionPerformed

    private void bhapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bhapusActionPerformed
        // TODO add your handling code here:
        if(!id_pemesanan.equals("")){
            int confirm = JOptionPane.showConfirmDialog(null, "Apakah anda yakin ingin menghapus data tersebut?", "Konfirmasi", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            try{
                if (confirm == 0) {
                    PreparedStatement stat = con.prepareStatement("delete from pemesanan where id_pemesanan='" + id_pemesanan + "'");
                    stat.executeUpdate();
                    JOptionPane.showMessageDialog(null, "Data berhasil dihapus", "Pesan", JOptionPane.INFORMATION_MESSAGE);
                    datatable();
                }
            }catch (SQLException e) {

            }            
        }else{
            JOptionPane.showMessageDialog(null, "Mohon pilih data di table terlebih dahulu", "Pesan", JOptionPane.INFORMATION_MESSAGE);            
        }
    }//GEN-LAST:event_bhapusActionPerformed

    private void bhapus1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bhapus1ActionPerformed
        // TODO add your handling code here:
        try{
            String RealPath = "lib/"; // TODO add your handling code here:
            HashMap hash = new HashMap();
            String NamaFile = "/report/pemesanan.jasper";
            Map<String, Object> map = new HashMap<>();
            String tgl_nolengkap = Util.dateIndonesia();
            map.put("tgl_nolengkap", tgl_nolengkap);                
            map.put("RealPath", RealPath);
            InputStream report;
            report = getClass().getResourceAsStream(NamaFile);            
            jasperPrint = JasperFillManager.fillReport(report, map, con);
            JasperViewer.viewReport(jasperPrint, false);
            new Master().setVisible(true);
            this.dispose();
        }catch(JRException e){
            
        } catch (ParseException ex) {
            Logger.getLogger(PemesananWo.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }//GEN-LAST:event_bhapus1ActionPerformed

    private void bkembaliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bkembaliActionPerformed
        // TODO add your handling code here:
        new Pemesanan().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_bkembaliActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PemesananWo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PemesananWo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PemesananWo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PemesananWo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
      java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                PemesananWo dialog = new PemesananWo(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });        
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bhapus;
    private javax.swing.JButton bhapus1;
    private javax.swing.JButton bkembali;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JTable tablevendor;
    private javax.swing.JTextField tcari;
    // End of variables declaration//GEN-END:variables
}
