/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wo;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author FITRI RHAMADANI
 */
public class Pemesanan extends javax.swing.JFrame {
    final Connection con = Koneksi.getKoneksi();
    ArrayList<String> vendorMua = new ArrayList<>();
    ArrayList<String> vendorPhoto = new ArrayList<>();
    ArrayList<String> vendorEntertaiment = new ArrayList<>();
    ArrayList<String> supplierDekorasi = new ArrayList<>();
    ArrayList<String> supplierCatering = new ArrayList<>();
    ArrayList<String> supplierGedung = new ArrayList<>();
    private int allowVendplier,totalall = 0;
    private String hargaKatering = "";
    private String id_dekorasi,id_catering,id_gedung,id_photographer,id_mua,id_hiburan;
    private String hargadekorasi,hargacatering,hargagedung,hargaphotographer,hargamua,hargahiburan = null;
    
    /**
     * Creates new form Booking
     */
    public Pemesanan() {
        initComponents();
        txttglpemesanan.getDateEditor().setEnabled(false);
        txttglpernikahan.getDateEditor().setEnabled(false);
        txttglpemesanan.getDateEditor().addPropertyChangeListener(
            new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent e) {
                    if ("date".equals(e.getPropertyName())) {                    
                        try {
                            String input = e.getNewValue().toString();
                            SimpleDateFormat parser = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyyy");
                            Date date;                            
                            date = parser.parse(input);
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                            String formattedDate = formatter.format(date);
                            LocalDate dates = LocalDate.parse(formattedDate);
                            LocalDate newDate = dates.plusMonths(1); 
                            txttglpernikahan.setDate(formatter.parse(newDate.toString()));
                            txttglpernikahan.getJCalendar().setMinSelectableDate(formatter.parse(newDate.toString()));

                        } catch (ParseException ex) {
                            Logger.getLogger(Pemesanan.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });
        tjml_catering.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                hitungTotal();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                hitungTotal();                
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                hitungTotal();                
            }
            public void hitungTotal(){
                String pax = tjml_catering.getText();
                if(!hargaKatering.equals("") && !pax.equals("")){
                    int total = Integer.parseInt(pax) * Integer.parseInt(hargaKatering);
                    tharga_catering.setText(Util.currencyID(total+ "")); 
                    hargacatering = total+"";
                    hitungTotalAkhir();
                }else{
                    hargacatering = null;
                    tharga_catering.setText("");
                    hitungTotalAkhir();
                }
            }
        });        
        initData();
        checkingAwal();
    }
    public void checkingAwal(){
        checkingDekorasi();
        checkingCatering(true);
        checkingGedung(true);
        checkingMua(true);
        checkingPhotographer(true);
        checkingHiburan(true);
        
    }

    public void initData(){
        ambil_pelanggan(false,null);
        ambil_pegawai();
        ambil_vendor(false, null, "photographer");
        ambil_vendor(false, null, "mua");
        ambil_vendor(false, null, "entertainment");   
        //supplier
        ambil_supplier(false, null, "dekorasi");   
        ambil_supplier(false, null, "catering");   
        ambil_supplier(false, null, "gedung");
        
        //
        allowVendplier++;
        ckdekorasi.setSelected(true);
    }
    
    public void ambil_pelanggan(boolean justOne, String id){
        try{
            String sql = null;
            if(justOne && id != null){
                sql = "select * from pelanggan WHERE id_pelanggan = '"+id+"'";                
            }else{
                sql = "Select * from pelanggan";
            }
            java.sql.Statement stat = con.createStatement();
            java.sql.ResultSet hasil = stat.executeQuery(sql);
            if(justOne == true){
                while(hasil.next()){
                    tnama_pelanggan.setText(hasil.getString("nama_pelanggan"));
                }                
            }else{
                jComboBox1.addItem("== Pilih Pelanggan ==");
                while(hasil.next()){
                    jComboBox1.addItem(hasil.getString("id_pelanggan"));
                }
                hasil.last();
                int jumlahdata = hasil.getRow();
                hasil.first();                
            }

        }catch(Exception e){
            System.out.println(e);
        }        
    }
    
    public void ambil_pegawai(){
        try{
            String sql = "Select * from pegawai";
            java.sql.Statement stat = con.createStatement();
            java.sql.ResultSet hasil = stat.executeQuery(sql);

                jComboBox8.addItem("== Pilih Pegawai ==");
                while(hasil.next()){
                    jComboBox8.addItem(hasil.getString("id_pegawai"));
                }
                hasil.last();
                int jumlahdata = hasil.getRow();
                hasil.first();
        }catch(Exception e){
            System.out.println(e);
        }        
    }    
    

    public void ambil_vendor(boolean justOne, String id, String kategori){
        try{
            String sql = null;
            if(justOne && id != null){
                sql = "select * from vendor WHERE nama_vendor= '"+id+"'";                
            }else{
                sql = "Select * from vendor WHERE kategori_vendor= '"+kategori+"'";
            }
            java.sql.Statement stat = con.createStatement();
            java.sql.ResultSet hasil = stat.executeQuery(sql);
            if(justOne == true){
                while(hasil.next()){
                    switch (kategori){
                        case "mua":
                            tharga_mua.setText(Util.currencyID(hasil.getString("harga_vendor")));
                            id_photographer = hasil.getString("id_vendor");
                            hargamua = hasil.getString("harga_vendor");
                            break;
                        case "photographer":
                            tharga_phto.setText(Util.currencyID(hasil.getString("harga_vendor")));
                            id_mua = hasil.getString("id_vendor");
                            hargaphotographer = hasil.getString("harga_vendor");
                            break;
                        case "entertainment":
                            tharga_hiburan.setText(Util.currencyID(hasil.getString("harga_vendor")));
                            id_hiburan = hasil.getString("id_vendor");
                            hargahiburan = hasil.getString("harga_vendor");
                            break;                            
                    }
                }                
            }else{
                switch (kategori) {
                    case "mua":
                        jComboBox6.addItem("== Pilih Vendor ==");                            
                        break;
                    case "photographer":
                        jComboBox5.addItem("== Pilih Vendor ==");
                        break;                        
                    case "entertainment":
                        jComboBox7.addItem("== Pilih Vendor ==");                          
                        break;
                    default:
                        break;
                }                
                while(hasil.next()){
                    switch (kategori) {
                        case "mua":
                            vendorMua.add(hasil.getString("id_vendor"));
                            jComboBox6.addItem(hasil.getString("nama_vendor"));
                            break;
                        case "photographer":
                            vendorPhoto.add(hasil.getString("id_vendor"));
                            jComboBox5.addItem(hasil.getString("nama_vendor"));
                            break;
                        case "entertainment":
                            vendorEntertaiment.add(hasil.getString("id_vendor"));                    
                            jComboBox7.addItem(hasil.getString("nama_vendor"));                            
                            break;
                        default:
                            break;
                    }
                }
                hasil.last();
                int jumlahdata = hasil.getRow();
                hasil.first();                
            }

        }catch(Exception e){
            System.out.println(e);
        }        
    } 
    
    public void ambil_supplier(boolean justOne, String id, String kategori){
        try{
            String sql = null;
            if(justOne && id != null){
                sql = "select * from supplier WHERE nama_supplier= '"+id+"'";                
            }else{
                sql = "Select * from supplier WHERE kategori_supplier= '"+kategori+"'";
            }
            java.sql.Statement stat = con.createStatement();
            java.sql.ResultSet hasil = stat.executeQuery(sql);
            if(justOne == true){
                while(hasil.next()){
                    switch (kategori){
                        case "dekorasi":
                            tharga_dekorasi.setText(Util.currencyID(hasil.getString("harga_supplier")));
                            id_dekorasi = hasil.getString("id_supplier");
                            hargadekorasi = hasil.getString("harga_supplier");
                            break;
                        case "catering":
                            tharga_catering.setText(Util.currencyID(hasil.getString("harga_supplier")));
                            hargaKatering = hasil.getString("harga_supplier");
                            id_catering = hasil.getString("id_supplier");
                            hargacatering = hasil.getString("harga_supplier");
                            break;
                        case "gedung":
                            tharga_gedung.setText(Util.currencyID(hasil.getString("harga_supplier")));
                            id_gedung = hasil.getString("id_supplier");
                            hargagedung = hasil.getString("harga_supplier");
                            break;                            
                    }
                }                
            }else{
                switch (kategori) {
                    case "dekorasi":
                        jComboBox2.addItem("== Pilih Supplier ==");                            
                        break;
                    case "catering":
                        jComboBox3.addItem("== Pilih Supplier ==");
                        break;                        
                    case "gedung":
                        jComboBox4.addItem("== Pilih supplier ==");                          
                        break;
                    default:
                        break;
                }                
                while(hasil.next()){
                    switch (kategori) {
                        case "dekorasi":
                            supplierDekorasi.add(hasil.getString("id_supplier"));
                            jComboBox2.addItem(hasil.getString("nama_supplier"));
                            break;
                        case "catering":
                            supplierCatering.add(hasil.getString("id_supplier"));
                            jComboBox3.addItem(hasil.getString("nama_supplier"));
                            break;
                        case "gedung":
                            supplierGedung.add(hasil.getString("id_supplier"));                    
                            jComboBox4.addItem(hasil.getString("nama_supplier"));                            
                            break;
                        default:
                            break;
                    }
                }
                hasil.last();
                int jumlahdata = hasil.getRow();
                hasil.first();                
            }

        }catch(SQLException e){
            System.out.println(e);
        }        
    }      
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jButton5 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        ckdekorasi = new javax.swing.JCheckBox();
        jCheckBox2 = new javax.swing.JCheckBox();
        jCheckBox3 = new javax.swing.JCheckBox();
        jComboBox2 = new javax.swing.JComboBox<>();
        jComboBox3 = new javax.swing.JComboBox<>();
        jComboBox4 = new javax.swing.JComboBox<>();
        tharga_dekorasi = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        tharga_catering = new javax.swing.JTextField();
        tharga_gedung = new javax.swing.JTextField();
        tjml_catering = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        tnama_pelanggan = new javax.swing.JTextField();
        jComboBox8 = new javax.swing.JComboBox<>();
        jLabel13 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txttglpernikahan = new com.toedter.calendar.JDateChooser();
        txttglpemesanan = new com.toedter.calendar.JDateChooser();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jCheckBox4 = new javax.swing.JCheckBox();
        jComboBox5 = new javax.swing.JComboBox<>();
        jCheckBox5 = new javax.swing.JCheckBox();
        jComboBox6 = new javax.swing.JComboBox<>();
        jCheckBox6 = new javax.swing.JCheckBox();
        jComboBox7 = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        tharga_phto = new javax.swing.JTextField();
        tharga_mua = new javax.swing.JTextField();
        tharga_hiburan = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        ttotal_harga = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(900, 770));
        setSize(new java.awt.Dimension(900, 770));

        jPanel1.setBackground(new java.awt.Color(204, 153, 254));
        jPanel1.setFont(new java.awt.Font("Arial Narrow", 1, 28)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Arial Narrow", 1, 30)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Pemesanan");

        jButton5.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jButton5.setText("X");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(285, 285, 285)
                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setForeground(new java.awt.Color(255, 255, 255));

        jLabel7.setFont(new java.awt.Font("Arial Narrow", 1, 20)); // NOI18N
        jLabel7.setText("Supplier");

        ckdekorasi.setText("Dekorasi");
        ckdekorasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ckdekorasiActionPerformed(evt);
            }
        });

        jCheckBox2.setText("Catering");
        jCheckBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox2ActionPerformed(evt);
            }
        });

        jCheckBox3.setText("Gedung");
        jCheckBox3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox3ActionPerformed(evt);
            }
        });

        jComboBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox2ActionPerformed(evt);
            }
        });

        jComboBox3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox3ActionPerformed(evt);
            }
        });

        jComboBox4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox4ActionPerformed(evt);
            }
        });

        tharga_dekorasi.setEditable(false);
        tharga_dekorasi.setText("Rp. ");
        tharga_dekorasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tharga_dekorasiActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Arial Narrow", 1, 20)); // NOI18N
        jLabel8.setText("Harga");
        jLabel8.setPreferredSize(new java.awt.Dimension(47, 24));
        jLabel8.setRequestFocusEnabled(false);

        tharga_catering.setEditable(false);
        tharga_catering.setText("Rp.");

        tharga_gedung.setEditable(false);
        tharga_gedung.setText("Rp.");

        tjml_catering.setEnabled(false);
        tjml_catering.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tjml_cateringActionPerformed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Arial Narrow", 1, 18)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Pax");

        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        tnama_pelanggan.setEditable(false);
        tnama_pelanggan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tnama_pelangganActionPerformed(evt);
            }
        });

        jComboBox8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox8ActionPerformed(evt);
            }
        });

        jLabel13.setText("Id Pegawai");

        jLabel3.setText("Nama Pelanggan");

        jLabel2.setText("Id Pelanggan");

        txttglpernikahan.setDateFormatString("yyyy-MM-dd");

        txttglpemesanan.setDateFormatString("yyyy-MM-dd");

        jLabel5.setText("Tanggal Pemesanan ");

        jLabel6.setText("Tanggal Pernikahan");

        jLabel4.setText("Pax");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addGap(59, 59, 59)
                        .addComponent(jComboBox8, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(116, 116, 116)
                        .addComponent(jLabel12)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jCheckBox2)
                                    .addComponent(jCheckBox3)
                                    .addComponent(ckdekorasi))
                                .addGap(30, 30, 30)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jComboBox2, 0, 226, Short.MAX_VALUE)
                                    .addComponent(jComboBox3, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jComboBox4, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(28, 28, 28)
                                .addComponent(tjml_catering, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel4))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(tnama_pelanggan)
                                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(tharga_catering, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(tharga_gedung, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(tharga_dekorasi, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(25, 25, 25))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel6))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txttglpemesanan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txttglpernikahan, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(38, 38, 38))))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(tnama_pelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jComboBox8, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13))
                        .addGap(18, 18, 18))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(txttglpemesanan, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel6)
                            .addComponent(txttglpernikahan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel12)))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(19, 19, 19)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tharga_dekorasi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ckdekorasi))
                .addGap(37, 37, 37)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBox2)
                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tjml_catering, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(tharga_catering, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jComboBox4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(tharga_gedung, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jCheckBox3))
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setForeground(new java.awt.Color(255, 255, 255));

        jLabel9.setFont(new java.awt.Font("Arial Narrow", 1, 20)); // NOI18N
        jLabel9.setText("Vendor");

        jCheckBox4.setText("Photographer");
        jCheckBox4.setActionCommand("");
        jCheckBox4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox4ActionPerformed(evt);
            }
        });

        jComboBox5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox5ActionPerformed(evt);
            }
        });

        jCheckBox5.setText("MUA");
        jCheckBox5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox5ActionPerformed(evt);
            }
        });

        jComboBox6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox6ActionPerformed(evt);
            }
        });

        jCheckBox6.setText("Hiburan");
        jCheckBox6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox6ActionPerformed(evt);
            }
        });

        jComboBox7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox7ActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Arial Narrow", 1, 20)); // NOI18N
        jLabel10.setText("Harga");

        tharga_phto.setEditable(false);
        tharga_phto.setText("Rp. ");
        tharga_phto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tharga_phtoActionPerformed(evt);
            }
        });

        tharga_mua.setEditable(false);
        tharga_mua.setText("Rp.");

        tharga_hiburan.setEditable(false);
        tharga_hiburan.setText("Rp.");

        jButton1.setText("Simpan");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setLabel("Kelola");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Arial Narrow", 1, 18)); // NOI18N
        jLabel11.setText("Total Harga :");

        ttotal_harga.setEditable(false);
        ttotal_harga.setText("Rp.");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel11)
                        .addGap(18, 18, 18)
                        .addComponent(ttotal_harga, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jCheckBox6)
                            .addComponent(jCheckBox5)
                            .addComponent(jCheckBox4))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jComboBox6, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jComboBox5, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jComboBox7, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tharga_hiburan, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tharga_mua, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addComponent(tharga_phto, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(28, 28, 28))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tharga_phto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(tharga_mua, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15)
                        .addComponent(tharga_hiburan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jComboBox5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jComboBox6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jComboBox7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel3Layout.createSequentialGroup()
                                    .addComponent(jCheckBox4)
                                    .addGap(18, 18, 18)
                                    .addComponent(jCheckBox5))
                                .addGroup(jPanel3Layout.createSequentialGroup()
                                    .addGap(88, 88, 88)
                                    .addComponent(jCheckBox6))))))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ttotal_harga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(45, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ckdekorasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ckdekorasiActionPerformed
        // TODO add your handling code here:
          checkingDekorasi();
    }//GEN-LAST:event_ckdekorasiActionPerformed

    private void jCheckBox4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox4ActionPerformed
        // TODO add your handling code here:
         checkingPhotographer(false);          
    }//GEN-LAST:event_jCheckBox4ActionPerformed

    private void jCheckBox5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox5ActionPerformed
        // TODO add your handling code here:
         checkingMua(false);          

    }//GEN-LAST:event_jCheckBox5ActionPerformed

    private void jCheckBox6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox6ActionPerformed
        // TODO add your handling code here:
         checkingHiburan(false);                         
    }//GEN-LAST:event_jCheckBox6ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        new Master().setVisible(true);
        dispose();        
    }//GEN-LAST:event_jButton5ActionPerformed

    private void tharga_phtoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tharga_phtoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tharga_phtoActionPerformed

    private void jComboBox5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox5ActionPerformed
        if(jComboBox5.getSelectedIndex() == 0){
            tharga_phto.setText("");
            hargaphotographer = null;
            hitungTotalAkhir();
        }else{
            ambil_vendor(true, jComboBox5.getSelectedItem().toString(),"photographer");
            hitungTotalAkhir();
        }// TODO add your handling code here:
    }//GEN-LAST:event_jComboBox5ActionPerformed

    private void tnama_pelangganActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tnama_pelangganActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tnama_pelangganActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        // TODO add your handling code here:
        if(jComboBox1.getSelectedIndex() == 0){
            tnama_pelanggan.setText("");
        }else{
            ambil_pelanggan(true, jComboBox1.getSelectedItem().toString());
        }
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jComboBox6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox6ActionPerformed
        if(jComboBox6.getSelectedIndex() == 0){
            tharga_mua.setText("");
            hargamua = null;
            hitungTotalAkhir();
        }else{
            ambil_vendor(true, jComboBox6.getSelectedItem().toString(),"mua");
            hitungTotalAkhir();
        }//         // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox6ActionPerformed

    private void jComboBox7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox7ActionPerformed
        // TODO add your handling code here:
        if(jComboBox7.getSelectedIndex() == 0){
            tharga_hiburan.setText("");
            hargahiburan = null;
            hitungTotalAkhir();
        }else{
            ambil_vendor(true, jComboBox7.getSelectedItem().toString(),"entertainment");
            hitungTotalAkhir();
        }
    }//GEN-LAST:event_jComboBox7ActionPerformed

    private void jComboBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox2ActionPerformed
        // TODO add your handling code here:
        if(jComboBox2.getSelectedIndex() == 0){
            tharga_dekorasi.setText("");
            hargadekorasi = null;
            hitungTotalAkhir();
        }else{
            ambil_supplier(true, jComboBox2.getSelectedItem().toString(),"dekorasi");
            hitungTotalAkhir();
        }        
    }//GEN-LAST:event_jComboBox2ActionPerformed

    private void jComboBox3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox3ActionPerformed
        // TODO add your handling code here:
        if(jComboBox3.getSelectedIndex() == 0){
            tharga_catering.setText("");
            tjml_catering.setEnabled(false);
            tjml_catering.setText("");     
            hargacatering = null;
            hitungTotalAkhir();
        }else{
            tjml_catering.setEnabled(true);
            tjml_catering.setText("1");            
            ambil_supplier(true, jComboBox3.getSelectedItem().toString(),"catering");
            hitungTotalAkhir();
        }                
    }//GEN-LAST:event_jComboBox3ActionPerformed

    private void jComboBox4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox4ActionPerformed
        // TODO add your handling code here:
        if(jComboBox4.getSelectedIndex() == 0){
            tharga_gedung.setText("");
            hargagedung = null;
            hitungTotalAkhir();
        }else{
            ambil_supplier(true, jComboBox4.getSelectedItem().toString(),"gedung");
            hitungTotalAkhir();
        }           
    }//GEN-LAST:event_jComboBox4ActionPerformed

    private void tharga_dekorasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tharga_dekorasiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tharga_dekorasiActionPerformed

    private void jCheckBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox2ActionPerformed
        // TODO add your handling code here:
        checkingCatering(false);        
    }//GEN-LAST:event_jCheckBox2ActionPerformed

    private void jCheckBox3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox3ActionPerformed
        // TODO add your handling code here:
        checkingGedung(false);           
    }//GEN-LAST:event_jCheckBox3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        try{
            int errCount = 0;
            if(tnama_pelanggan.getText().equals("")){
                JOptionPane.showMessageDialog(null, "Mohon pilih pelanggan terlebih dahulu!");        
            }else if(jComboBox8.getSelectedIndex() == 0){
                JOptionPane.showMessageDialog(null, "Mohon pilih Data Pegawai terlebih dahulu!");
            }else if(txttglpemesanan.getDate() == null){
                JOptionPane.showMessageDialog(null, "Mohon isi Tanggal pemesanan terlebih dahulu!");
            }else if(txttglpernikahan.getDate() == null){
                JOptionPane.showMessageDialog(null, "Mohon isi Tanggal Pernikahan terlebih dahulu!");
            }else if(tharga_dekorasi.getText().equals("")){
                JOptionPane.showMessageDialog(null, "Mohon isi supplier dekorasi terlebih daulu!");
            }else if(allowVendplier < 2){
                JOptionPane.showMessageDialog(null, "mohon pilih 1 vendor atau supplier selain dekorasi!");        
            }else{
                if(jCheckBox2.isSelected()){
                    if(tharga_catering.getText().equals("")){
                        errCount++;
                        JOptionPane.showMessageDialog(null, "Mohon isi supplier catering terlebih daulu!");                    
                    }
                }
                if(jCheckBox3.isSelected()){
                    if(tharga_gedung.getText().equals("")){
                        errCount++;                        
                        JOptionPane.showMessageDialog(null, "Mohon isi supplier gedung terlebih daulu!");                    
                    }
                }
                if(jCheckBox4.isSelected()){
                    if(tharga_phto.getText().equals("")){
                        errCount++;                        
                        JOptionPane.showMessageDialog(null, "Mohon isi vendor photo terlebih daulu!");                    
                    }
                }
                if(jCheckBox5.isSelected()){
                    if(tharga_mua.getText().equals("")){
                        errCount++;                        
                        JOptionPane.showMessageDialog(null, "Mohon isi vendor mua terlebih daulu!");                    
                    }
                }
                if(jCheckBox6.isSelected()){
                    if(tharga_hiburan.getText().equals("")){
                        errCount++;                        
                        JOptionPane.showMessageDialog(null, "Mohon isi vendor hiburan terlebih daulu!");                    
                    }
                }
                if(errCount == 0){
                    PreparedStatement stat = con.prepareStatement("insert into pemesanan (id_pemesanan,tgl_pemesanan,tgl_pernikahan,id_pelanggan,id_pegawai,id_gedung,id_dekorasi, id_catering,porsi_catering,id_fotografer,id_mua,id_entertaiment,total) values (?,?,?,?,?,?,?,?,?,?,?,?,?)");
                    stat.setString(1, "PSN-"+Util.getSaltString());
                    stat.setString(2, ((JTextField)txttglpemesanan.getDateEditor().getUiComponent()).getText());
                    stat.setString(3, ((JTextField)txttglpernikahan.getDateEditor().getUiComponent()).getText());
                    stat.setString(4, jComboBox1.getSelectedItem().toString());
                    stat.setString(5, jComboBox8.getSelectedItem().toString());
                    stat.setString(6, id_gedung);
                    stat.setString(7, id_dekorasi);
                    stat.setString(8, id_catering);
                    stat.setString(9, !tjml_catering.getText().equals("")? tjml_catering.getText() : "0");
                    stat.setString(10,id_photographer);
                    stat.setString(11, id_mua);
                    stat.setString(12,id_hiburan);                    
                    stat.setString(13,totalall+"");                                        

                    stat.executeUpdate();
                    JOptionPane.showMessageDialog(null, "Data berhasil disimpan", "Pesan", JOptionPane.INFORMATION_MESSAGE);
                    JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
                    new PemesananWo(topFrame,true).setVisible(true);
                    this.dispose();
                }else{
                    JOptionPane.showMessageDialog(null, "Gagal", "Pesan", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        }catch(Exception e){
         System.out.println(e);   
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    
    private void tjml_cateringActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tjml_cateringActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tjml_cateringActionPerformed

    private void jComboBox8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox8ActionPerformed
        // TODO add your handling code here: 
    }//GEN-LAST:event_jComboBox8ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
        new PemesananWo(topFrame,true).setVisible(true);
        dispose();
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    public void hitungTotalAkhir(){
        int total = 0;
        if(hargadekorasi != null){
            total+= Integer.parseInt(hargadekorasi);
        }
        if(jCheckBox2.isSelected()){
            if(hargacatering != null){
                total+= Integer.parseInt(hargacatering);
            }
        }
        if(jCheckBox3.isSelected()){
            if(hargagedung != null){
                total+= Integer.parseInt(hargagedung);
            }
        }
        if(jCheckBox4.isSelected()){
            if(hargaphotographer != null){
                total+= Integer.parseInt(hargaphotographer);
            }
        }
        if(jCheckBox5.isSelected()){
            if(hargamua != null){
                total+= Integer.parseInt(hargamua);
            }
        }
        if(jCheckBox6.isSelected()){
            if(hargahiburan != null){
                total+= Integer.parseInt(hargahiburan);
            }
        }
        totalall = total;
        ttotal_harga.setText(Util.currencyID(total +""));
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Pemesanan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Pemesanan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Pemesanan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Pemesanan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Pemesanan().setVisible(true);
            }
        });
    }
    //enable disabled checkbox
    public void checkingDekorasi(){
        if(!ckdekorasi.isSelected()){
            JOptionPane.showMessageDialog(null, "dekorasi wajib disertakan!");            
            ckdekorasi.setSelected(true);
        }              
    }
    public void checkingCatering(boolean firstTime){
        if(jCheckBox2.isSelected()){
            allowVendplier++;
            jComboBox3.setEnabled(true);
            tharga_catering.setEnabled(true);            
        }else{
            if(firstTime == false){
                allowVendplier--;
            }
            jComboBox3.setEnabled(false);
            jComboBox3.setSelectedIndex(0);
            tjml_catering.setEnabled(false);
            tjml_catering.setText("");            
            tharga_catering.setEnabled(false);             
            tharga_catering.setText("");             
        }                        
    }
    public void checkingGedung(boolean firstTime){
        if(jCheckBox3.isSelected()){
            allowVendplier++;
            jComboBox4.setEnabled(true);
            tharga_gedung.setEnabled(true);            
        }else{
            if(firstTime == false){
                allowVendplier--;
            }
            jComboBox4.setEnabled(false);
            jComboBox4.setSelectedIndex(0);
            tharga_gedung.setEnabled(false);             
            tharga_gedung.setText("");             
        }           
    }
    public void checkingMua(boolean firstTime){
        if(jCheckBox5.isSelected()){
            allowVendplier++;
            jComboBox6.setEnabled(true);
            tharga_mua.setEnabled(true);            
        }else{
            if(firstTime == false){
                allowVendplier--;
            }
            jComboBox6.setEnabled(false);
            jComboBox6.setSelectedIndex(0);
            tharga_mua.setEnabled(false);             
            tharga_mua.setText("");             
        }                
    }
    public void checkingPhotographer(boolean firstTime){
        if(jCheckBox4.isSelected()){
            allowVendplier++;
            jComboBox5.setEnabled(true);
            tharga_phto.setEnabled(true);            
        }else{
            if(firstTime == false){
                allowVendplier--;
            }
            jComboBox5.setEnabled(false);
            jComboBox5.setSelectedIndex(0);
            tharga_phto.setEnabled(false);             
            tharga_phto.setText("");             
        }            
    }
    public void checkingHiburan(boolean firstTime){
        if(jCheckBox6.isSelected()){
            allowVendplier++;
            jComboBox7.setEnabled(true);
            tharga_hiburan.setEnabled(true);            
        }else{
            if(firstTime == false){
                allowVendplier--;
            }
            jComboBox7.setEnabled(false);
            jComboBox7.setSelectedIndex(0);
            tharga_hiburan.setEnabled(false);             
            tharga_hiburan.setText("");             
        }         
    }    
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox ckdekorasi;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton5;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JCheckBox jCheckBox3;
    private javax.swing.JCheckBox jCheckBox4;
    private javax.swing.JCheckBox jCheckBox5;
    private javax.swing.JCheckBox jCheckBox6;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JComboBox<String> jComboBox3;
    private javax.swing.JComboBox<String> jComboBox4;
    private javax.swing.JComboBox<String> jComboBox5;
    private javax.swing.JComboBox<String> jComboBox6;
    private javax.swing.JComboBox<String> jComboBox7;
    private javax.swing.JComboBox<String> jComboBox8;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JTextField tharga_catering;
    private javax.swing.JTextField tharga_dekorasi;
    private javax.swing.JTextField tharga_gedung;
    private javax.swing.JTextField tharga_hiburan;
    private javax.swing.JTextField tharga_mua;
    private javax.swing.JTextField tharga_phto;
    private javax.swing.JTextField tjml_catering;
    private javax.swing.JTextField tnama_pelanggan;
    private javax.swing.JTextField ttotal_harga;
    private com.toedter.calendar.JDateChooser txttglpemesanan;
    private com.toedter.calendar.JDateChooser txttglpernikahan;
    // End of variables declaration//GEN-END:variables
}
